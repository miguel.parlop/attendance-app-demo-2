class OutputFile():


    def __init__(self, target_path, file_name):
        self.target_path = target_path
        self.file_name = file_name
        self.rows = []
    

    def set_target_path(self,path):
        self.target_path = path


    def add_row(self, row):
        """
        it appends row: list in rows:list atribute
        each element in row: list is converted to str and then elements are joined by comma
        """
        self.rows.append(",".join([str(element)for element in row]))


    def write_file(self, mode='w'):
        """
        writes rows:list atribute in target_path:str and file_name: str 
        each row is joined by '\n' escape character

        The caller is responsible of making sure target_path exists, otherwise 
        this method will throw exception; 
        caller needs to be prepared to handle exception generate by open and write calls
        """

        # TODO: add a private method to generate file path
        # by using os.path function instead of f'{self.target_path}/{self.file_name}
        
        with open(f'{self.target_path}/{self.file_name}', mode) as file:
            print(self.rows)
            file.write("\n".join(self.rows))



class SuccessFile(OutputFile):

    def __init__(self, target_path, file_name):
        super().__init__(target_path, file_name)



class ErrorFile(OutputFile):

    def __init__(self, target_path, file_name):
        super().__init__(target_path, file_name)


    def add_row(self, file_name):
        self.rows.append(file_name)
    

    def write_file(self):
        if len(self.rows) == 0:
            self.rows = ["0"]
            
        super().write_file()
