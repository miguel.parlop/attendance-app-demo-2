from os import listdir
from os.path import isfile, join
import csv
import sys
import os

from src.report_section_helper import *
from src.summary_builder import normalize_summary
from .helpers import subtract_time
from .helpers import subtract_time_strType
from src.logger_wrapper import setup_logger

from src.participant_builder import participants_email_none,participants_no_repeat,participants_repeat,get_list_all_participants,normalize_participant, resolve_participant_join_last_time

# CREATE FOLDER
from src.tools_helper import create_staged_folder, create_folder_with_current_date, create_output_csv_file, build_succes_folder, build_error_folder, create_path


from src.OutputFile import SuccessFile, ErrorFile


logger = setup_logger(__name__)


success_file = SuccessFile('','success.txt')
error_file = ErrorFile('','error.txt')


MEETING_SUMMARY = 'Meeting Summary'
FULL_NAME =  'Full Name'
PARTICIPANTS  = '2. Participants'
ACTIVITIES = '3. In-Meeting activities'
STAGE_PATH_NAME = 'staged_data_folder'
EMAIL = 'Email'


# 3 file with conditions reached
def file_meet_conditions(path, pattern, file_path):
    return isfile(join(path, file_path)) and file_path.endswith(pattern)


def get_just_name_file(path):
    file_path_name_csv = path.split("\\")[-1] if "\\" in path else path.split("/")[-1]
    return file_path_name_csv[:-4]
    print(file_path_name_csv[:-4])


#  2 discovery de "data" files
def discover_data_files(path='../data', pattern='csv'):
    logger.info(f"Discoverng {pattern} data in {path} directory")
    try:
        return [join(path, f) for f in listdir(path) if file_meet_conditions(path, pattern, f)]
    except Exception as e:
        logger.exception(f"new error ocurrend when discovergin {pattern} in this directory {path}, {e}")


def get_file_data(path):
    logger.info(f"Discoverng data from {path}")
    try:
        with open(path, encoding='UTF-16') as reader:
            return list(csv.reader(reader, delimiter='\t'))
    except Exception as e:
        logger.exception(f"New error ocurrend when discovergin data from {path}, {e}")
        

def get_summary_as_dict(summary):
    # convert list that contains "summary" lines into dict 
    summary_dict = {}
    for element in summary:
        _, row = element
        # TODO must guarante index 0 and 1 exist
        summary_dict[row[0]] = row[1]
    return summary_dict


def get_data_as_dict(data):
    if data == []:
        return data

    result = []
    _, header = data[0] # TODO must guarante index 0 exists
    for _, row in data:
        result.append(dict(zip(header,row)))
    return result


def get_participant_as_dict(data):
    if data == []:
        return data

    result = []
    _, header = data[0] # TODO must guarante index 0 exists
    for _, row in data:
        result.append(dict(zip(header,row)))
    return result


def get_in_meeting_as_dict(data):
    if data == {}:
        return data

    result = []
    _, header = data[0]
    for element in data:
        _, row = element
        # TODO must guarante index 0 and 1 exist
        result.append(dict(zip(header,row)))
    return result


def get_section(data, section): # (data, 'summary')
    logger.info(f"Spliting sections for {section}")
    try:
        is_old_file_version = [MEETING_SUMMARY] in data
        summary =  extract_section_rows(data, MEETING_SUMMARY, FULL_NAME) if is_old_file_version else extract_section_rows(data, '1. Summary', PARTICIPANTS)
        participants  = [] if is_old_file_version else extract_section_rows(data, PARTICIPANTS, ACTIVITIES)
        in_meetings = extract_section_rows(data, FULL_NAME, include_start=True) if is_old_file_version else extract_section_rows(data, ACTIVITIES)

        if section == 'Summary': return summary
        if section == 'Participants': return participants
        if section == 'In_meeting_activities': return in_meetings
    
    except Exception as e:
        logger.exception(f"Error when trying to split section in {section} section, type error: {e}")


def normalize_old_version(list_participants,key = EMAIL):

    list_participants_email_none = participants_email_none(list_participants)
    list_participants_no_repeat = participants_no_repeat(list_participants)
    list_participants_repeat_raw_data = participants_repeat(list_participants)

    big_list_participants = []

    list_names_of_list_repeat = list(set(get_list_all_participants(list_participants_repeat_raw_data)))


    for participant in list_participants_no_repeat:
        big_list_participants.append(normalize_participant(participant))

    for _,participant in enumerate(list_names_of_list_repeat):

        list_one_participant = [p_r for p_r in list_participants_repeat_raw_data  if participant == p_r.get(key)]

        big_list_participants.append(resolve_participant_join_last_time(list_one_participant))

    
    for participant in list_participants_email_none:
        big_list_participants.append(normalize_participant(participant))


    return big_list_participants


def normalize_new_version(list_participants):
    return [normalize_participant(participant) for participant in list_participants]


def normalize_participants(list_participants, list_in_meeting_participant):
    normalized_participant_list= []
    if list_participants == []:
        normalized_participant_list = normalize_old_version(list_in_meeting_participant)
    else:
        normalized_participant_list = normalize_new_version(list_participants)
    return normalized_participant_list


def normalize_in_meeting_activities(in_meeting_activities_data: dict):
    normalized_in_meeting = []
    for element in in_meeting_activities_data:
        normalized_in_meeting.append(get_data_meetings_list(element))
    
    return normalized_in_meeting


def get_data_meetings_list(in_meeting_activities_data):
    # print(in_meeting_activities_data)
    Name = in_meeting_activities_data.get('Name') or in_meeting_activities_data.get('Full Name')
    Join_time = in_meeting_activities_data.get('Join time') or in_meeting_activities_data.get('Join Time')
    Leave_time = in_meeting_activities_data.get('Leave time') or in_meeting_activities_data.get('Leave Time')
    Duration = subtract_time_strType(Join_time, Leave_time)  
    Email = in_meeting_activities_data.get('Email')
    Role =  in_meeting_activities_data['Role']
    
    return {
            'Name': Name,
            'Join_time': Join_time,
            'Leave_time': Leave_time,
            'Duration': Duration,
            'Email': Email,
            'Role': Role
    }


def normalize_meeting_by_section(sec_su, sec_par, sec_in_me):
    #print(sec_in_me)
    
    summary_dict = get_summary_as_dict(sec_su)
    participant_dict = get_participant_as_dict(sec_par)
    in_meeting_dict = get_in_meeting_as_dict(sec_in_me)
    #print("sumary dictionary ", sumary_dict)
    #print("meeting dictionary ",in_meeting_dict)
    
    sumary = normalize_summary(summary_dict)
    in_meeting_activities = normalize_in_meeting_activities(in_meeting_dict[1::])
    participants = normalize_participants(participant_dict[1::], in_meeting_dict[1::])

    return {"summary" : sumary , "participants" : participants, "in_meeting_activities" : in_meeting_activities}


def do_file_processing(name_file, data, folder_path, staged_current_date_path):
    logger.info(f"Initial file processor for each data file")
    name_csv_proccesed = name_file # [meeting tile, nombreasdf adfa df]
    try: 
        sumary_section = get_section(data, 'Summary')
        participants_section = get_section(data, 'Participants')
        in_meeting_activities_section = get_section(data,'In_meeting_activities')

        normalize_attendance_meeting = normalize_meeting_by_section(sumary_section, participants_section, in_meeting_activities_section)
        
        # Create new csv file with data normalized
        create_output_csv_file(normalize_attendance_meeting, folder_path, staged_current_date_path)
        total_participants = normalize_attendance_meeting.get('summary').get("Attended participants")
        file_path_name_csv = folder_path.split("\\")[-1] if "\\" in folder_path else folder_path.split("/")[-1]
        #build_succes_folder(total_participants, staged_current_date_path, file_path_name_csv)
        # build_error_folder(staged_current_date_path, file_path_name_csv, False)
        success_file.add_row([total_participants, name_csv_proccesed])
        
    except Exception as e:
        logger.exception(f"Error ocurrent trying to write in file: {folder_path}, erroy type: {e}")
        #build_error_folder(staged_current_date_path, folder_path, True)
        
        error_file.add_row(name_csv_proccesed)


def load_data():
    file_paths = discover_data_files() or []
    create_staged_folder()

    current_date_path_name = create_folder_with_current_date(STAGE_PATH_NAME)
    
    new_path_file = create_path(current_date_path_name)

    success_file.set_target_path(new_path_file)
    error_file.set_target_path(new_path_file)

    if len(file_paths) == 0:
        logger.warning("There isn't any files to can be process, review the principal folder data")
        build_succes_folder(0, current_date_path_name, '', False)
        build_error_folder(current_date_path_name, '')
        
    else:    
        # print("LOAD DATA file_paths ",len(file_paths))
        logger.debug("Iterate and send every single file .csv")
        # create folder with current date

        for fp in file_paths:
            data = get_file_data(fp)
            name_file = get_just_name_file(fp)
            do_file_processing(name_file, data, fp, current_date_path_name)
        
        logger.debug("End execution")

    success_file.write_file()
    error_file.write_file()

    return len(file_paths)
        
            
            
