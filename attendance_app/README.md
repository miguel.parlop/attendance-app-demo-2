# Attendances app

## Getting started

Repository Link:

# Attendance app Demo

## DESCRIPTION

THIS APPLICACION TOOL WAS DESIGNED TO NORMALIZE AN OUTPUT FROM TWO DIFFRENT KIND OF CSV DATA FILES
CALLED V1 VERSION AND V2 VERSION
THE TOOL WILL SET A STANDAR OUTPUT INTO NEW CSV FILES THAT WILL BE SAVED INTO A
NEW FOLDER CALLED "STAGED_DATA_FOLDER"

- Each execution of the tool generates a new folder which name is in this
  format YYYYMMDDHHMMSS for example 20221213130000

- Each execution of the tool generates a success.txt that contains a summary
  of all files that were read and process successfully. Each entry contains 2
  values separated by comma: number of participants and relative file path

- Each execution of the tool generates an error.txt that contains a summary
  of all files that failed to be processed. If errors were not found this file has
  one line withvalue of 0

## RECOMMENDATIONS

- Entry point and Run Command ..\attendance_app\tools> py .\file_converter.py
- PYTHON VERSION RECOMMENDED 3.9.14
- we used pretty table lib for the final console output to show  a summary of the process that has been made by tools app
    pip install prettytable



## --------------------------------------------------------------

## DEVELOPERS TEAM

Diego Quisbert Gutierrez
Alejandra Maldonado
Israel Zurita Sanchez
